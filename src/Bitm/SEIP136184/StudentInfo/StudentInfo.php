<?php

namespace App\Bitm\SEIP136184\StudentInfo;

use App\Bitm\SEIP136184\Message\Message;
use App\Bitm\SEIP136184\Utility\Utility;

class StudentInfo{
    public $id= '';
    public $fullName = '';
    public $courseName='';
    public $conn;


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost","root","","labxm6b22");
    }


    public function prepare($data){
        if(array_key_exists("id",$data))
            $this->id= $data['id'];

        if(array_key_exists("fullName",$data))
            $this->fullName= $data['fullName'];
        if(array_key_exists("courseName",$data))
            $this->courseName= $data['courseName'];
    }

    public function store(){
        $query = "INSERT INTO `labxm6b22`.`student_information` (`id`, `fullName`, `courseName`) VALUES ('".$this->id."', '".$this->fullName."', '".$this->courseName."')";
        $result = mysqli_query($this->conn,$query);
        if($result){
            Message::message("Data has been stored successfully");
            Utility::redirect("index.php");
        }

        else
            echo "ERROR!";
    }
    public function index(){
        $student_information= array();
        $query = "SELECT * FROM `student_information` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $student_information[]=$row;
        return $student_information;
    }
    public function view()
    {

    }



}