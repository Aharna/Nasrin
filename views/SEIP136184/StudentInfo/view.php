<?php
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136184\StudentInfo\StudentInfo;
use App\Bitm\SEIP136184\Message\Message;
use App\Bitm\SEIP136184\Utility\Utility;

$obj = new StudentInfo();
$obj->prepare($_GET);
$Hobby = $obj->view();

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Submitting Subscriber Hobby</title>

</head>
<body>

<div class="container">
    <h2>Select Your Hobbies</h2>
    <form role="form">

        <div class="checkbox">
            <label><input type="checkbox" name="courseName[]" disabled value="php" <?php if(in_array("php",$Course)) : ?> checked <?php endif; ?> >php</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="courseName[]" disabled value="java" <?php if(in_array("java",$Course)) : ?> checked <?php endif; ?>>java</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="courseName[]" disabled value="phython" <?php if(in_array("phython",$Course)) : ?> checked <?php endif; ?> >phython</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="courseName[]" disabled value="oracle" <?php if(in_array("oracle",$Course)) : ?> checked <?php endif; ?> >oracle</label>
        </div>
        <a href="index.php" class="btn btn-info" role="button">Done</a>
    </form>
</div>

</body>
</html>
