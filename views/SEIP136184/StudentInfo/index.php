<?php

session_start();

include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136184\StudentInfo\StudentInfo;
use App\Bitm\SEIP136184\Message\Message;
use App\Bitm\SEIP136184\Utility\Utility;


$obj =new \App\Bitm\SEIP136184\StudentInfo\StudentInfo();
$obj->prepare($_POST);
$allCourseName = $obj->index();

?>



<!DOCTYPE html>
<html>
<head>

</head>
<body>

<div class="container">
    <h2>Students info</h2>


    <a href="create.php" class="btn btn-primary" role="button">Create Again</a>
    <a href="trashlist.php" class="btn btn-primary" role="button">View Trash List</a>

    <div class="alert alert-info" id="message">
        <?php
            if(array_key_exists("message",$_SESSION) && (!empty($_SESSION['message'])))
            echo Message::message()
            ?>
    </div>

    <table>
        <thead>
        <tr>
            <th>SL</th>
            <th>id</th>
            <th>fullName</th>
            <th>courseName</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $SL=0;
        foreach ($allcourseName as $student) {
            $SL++;
            ?>
        <tr>

            <td> <?php echo $student->id ?> </td>
            <td> <?php echo $student->fullName ?> </td>
            <td> <?php echo $student->courseName ?> </td>
            <td>
                <a href="view.php?id=<?php echo $student->id?>"  role="button">View</a>
                <a href="edit.php?id=<?php echo $student->id?>"role="button">Update</a>
                <a href="trash.php?id=<?php echo $student->id?>" role="button">Trash</a>
                <a href="delete.php?id=<?php echo $student->id?>"  role="button" Onclick="return ConfirmDelete()">Delete</a>
            </td>
        </tr>
       <?php } ?>
        </tbody>
    </table>
</div>

        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }



</body>
</html>